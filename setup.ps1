$pythonPath = Get-Command python -ErrorAction SilentlyContinue

if ($pythonPath) {
    Write-Output "Using Python at $($pythonPath.Path)"
} else {
    Write-Output "Python is not installed or not found in the system PATH."
    exit
}

if (-not (Test-Path -Path ".\venv" -PathType Container)) {
    Write-Output "Setting up venv"
    python -m venv venv
    Write-Output "Updating pip"
    python.exe -m pip install --upgrade pip
}

& "venv\Scripts\Activate.ps1"

Write-Output "Installing pytorch"
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu124

Write-Output "Installing other dependencies"
pip3 install tqdm scipy

Write-Output "Starting segmentation"
python .\test.py
Write-Output "Finished, segmented pointcloud saved to data\saved_models\toronto3d\output\segmentations\birmingham"