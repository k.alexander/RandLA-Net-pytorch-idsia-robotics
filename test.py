import segment

if __name__ == '__main__':
    segment.segment_point_cloud("data/saved_models/toronto3d/checkpoints/96_v_acc=0.895_v_iou=0.6278803512129257_state_dict.pth",
                    "data/birmingham/birmingham_block_12.ply",
                    "data/birmingham/segmented.ply")
