"""
    Prepares a generic ply file into the datastructure used by this project
"""
from os.path import join
import os, sys
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

sys.path.append(BASE_DIR)
sys.path.append(os.path.join(BASE_DIR, 'utils'))

import grid_subsampling as cpp_subsampling # only works on windows
#import utils.cpp_subsampling.grid_subsampling as cpp_subsampling # only works on linux
import helper_ply as hp
import numpy as np
from model.hyperparameters import hyp
from pathlib import Path
from glob import glob
import pickle

"""
0: Unclassified
1: Road
2: Road_markings
3: Natural
4: Building
5: Utility_line
6: Pole
7: Car
8: Fence
"""
classes = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
UTM_OFFSET = [0, 0, 0]
file = Path(sys.argv[1])

def subsample(points, features, labels):
    return cpp_subsampling.compute(points, features=features, classes=labels, sampleDl=hyp["sub_grid_size"])

pc_id = 1
print(f"Processing {file}")

out_folder = file.parent / f"pc_id={pc_id}"
metadata_folder = out_folder / "metadata"

# create the folder if it does not exist
out_folder.mkdir(parents=True, exist_ok=True)
metadata_folder.mkdir(parents=True, exist_ok=True)

load_labels = False

# check if the file is already processed
if (out_folder / "pc.pickle").exists() :
    print(f"{file} is already subsampled. Skipping...")
else:
    ply = hp.read_ply(file)
    xyz =  np.vstack((ply['x'] - UTM_OFFSET[0], ply['y'] - UTM_OFFSET[1], ply['z'] - UTM_OFFSET[2])).T
    colors = np.vstack((ply['red'], ply['green'], ply['blue'])).T
    if load_labels and "scalar_Label" in ply.dtype.names:
        labels = ply['scalar_class']
    elif load_labels and "scalar_class" in ply.dtype.names:
        labels = ply['label']
    else:
        labels = np.zeros(xyz.shape[0])
    
    # subsample the point cloud
    print(f"Subsampling {file}")
    sub_xyz, sub_colors, sub_labels = subsample(xyz.astype(np.float32), colors, labels.astype(np.int32))
    sub_colors = sub_colors / 255.0

    print(f"Saving {file}")
    pc = np.concatenate((sub_xyz, sub_colors, sub_labels), axis=1, dtype=np.float32)

    # save the point cloud as pickle
    with open(out_folder / "pc.pickle", "wb") as f:
        pickle.dump(pc, f)

metadata = {
    "name": file.stem,
    "pc_id": pc_id,
    "sub_grid_size": hyp["sub_grid_size"],
    "UTM_OFFSET": UTM_OFFSET,
    "labels": classes
}

# save the metadata as pickle
with open(metadata_folder / "metadata.pickle", "wb") as f:
    pickle.dump(metadata, f)

