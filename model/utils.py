import os
import pickle
from scipy.spatial.transform import Rotation as R
import torch.nn as nn
import numpy as np


DATA_ROOT_PATH = 'data/'
MODEL_SAVES_PATH = DATA_ROOT_PATH + "saved_models/"

def check_create_folder(folder_path):
    if not os.path.exists(os.path.dirname(folder_path)):
        try:
            os.makedirs(os.path.dirname(folder_path))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise


def create_metadata(path, **kwargs):
    """
    Creates the metadata file at path/metadata/metadata.pkl

    Args:
        path: path to metadata folder
        kwargs: each kwarg is stored in the dict saved as pickle file

    """
    meta_path = f'{path}metadata/'
    check_create_folder(meta_path)
    with open(f'{meta_path}metadata.pickle', 'wb') as f:
        d = {x[0]: x[1] for x in kwargs.items()}
        pickle.dump(d, f)
    print(f'Metadata Stored : {d}')


def read_metadata(path):
    """
    Read the metadata file at path/metadata/metadata.pkl

    Args:
        path: path to metadata folder

    Returns:
        dict contained in metadata file

    """
    meta_file = f'{path}metadata/metadata.pickle'
    with open(meta_file, 'rb') as f:
        return pickle.load(f)

def rotate(points, angles):
    """
        Rotates a set of point around 'xyz' for the angles given in radians.

    Args:
        points: array of 3d points, with shape (n_points, 3)
        angles: angles[0]: rotation around x
                angles[1]: rotation around y
                angles[2]: rotation around z

    Returns:
        rotated points

    """
    r = R.from_euler('xyz', angles, degrees=False)
    return r.apply(points)

def unpack_input(input_list, n_layers, device):
    inputs = dict()
    inputs['xyz'] = input_list[:n_layers]
    inputs['neigh_idx'] = input_list[n_layers: 2 * n_layers]
    inputs['sub_idx'] = input_list[2 * n_layers:3 * n_layers]
    inputs['interp_idx'] = input_list[3 * n_layers:4 * n_layers]
    for key, val in inputs.items():
        inputs[key] = [x.to(device) for x in val]
    inputs['features'] = [input_list[4 * n_layers].to(device)]
    inputs['labels'] = [input_list[4 * n_layers + 1].to(device)]
    inputs['input_inds'] = [input_list[4 * n_layers + 2].to(device)]
    inputs['cloud_inds'] = [input_list[4 * n_layers + 3].to(device)]
    return inputs

def pack(r, g, b):
    """
        (r,g,b) tuple to hex. Each r,g,b can be column arrays

    """
    return (
            (np.array(r).astype(np.uint32) << 16)
            + (np.array(g).astype(np.uint32) << 8)
            + np.array(b).astype(np.uint32)
    )


def pack_single(rgb):
    """
        [r,g,b] one line array to hex
    """
    return (
            (rgb[0] << 16)
            + (rgb[1] << 8)
            + rgb[2]
    )